diff -uNr a/include/linux/fs.h b/include/linux/fs.h
--- a/include/linux/fs.h	2015-06-29 19:05:39.566427253 +0300
+++ b/include/linux/fs.h	2015-06-29 19:01:52.321424809 +0300
@@ -1331,6 +1337,11 @@
 	struct list_lru		s_dentry_lru ____cacheline_aligned_in_smp;
 	struct list_lru		s_inode_lru ____cacheline_aligned_in_smp;
 	struct rcu_head		rcu;
+
+	/*
+	 * Indicates how deep in a filesystem stack this SB is
+	 */
+	int s_stack_depth;
 };
 
 extern struct timespec current_fs_time(struct super_block *sb);
@@ -1463,6 +1474,7 @@
 extern int vfs_rmdir(struct inode *, struct dentry *);
 extern int vfs_unlink(struct inode *, struct dentry *, struct inode **);
 extern int vfs_rename(struct inode *, struct dentry *, struct inode *, struct dentry *, struct inode **);
+extern int vfs_whiteout(struct inode *, struct dentry *);
 
 /*
  * VFS dentry helper functions.
@@ -1696,6 +1708,9 @@
 #define IS_AUTOMOUNT(inode)	((inode)->i_flags & S_AUTOMOUNT)
 #define IS_NOSEC(inode)		((inode)->i_flags & S_NOSEC)
 
+#define IS_WHITEOUT(inode)	(S_ISCHR(inode->i_mode) && \
+				 (inode)->i_rdev == WHITEOUT_DEV)
+
 /*
  * Inode state bits.  Protected by inode->i_lock
  *
diff -uNr a/include/linux/rcupdate.h b/include/linux/rcupdate.h
--- a/include/linux/rcupdate.h	2014-03-31 07:40:15.000000000 +0400
+++ b/include/linux/rcupdate.h	2015-06-29 19:05:06.556426898 +0300
@@ -555,6 +555,21 @@
 #define RCU_INITIALIZER(v) (typeof(*(v)) __force __rcu *)(v)
 
 /**
+ * lockless_dereference() - safely load a pointer for later dereference
+ * @p: The pointer to load
+ *
+ * Similar to rcu_dereference(), but for situations where the pointed-to
+ * object's lifetime is managed by something other than RCU.  That
+ * "something other" might be reference counting or simple immortality.
+ */
+#define lockless_dereference(p) \
+({ \
+	typeof(p) _________p1 = ACCESS_ONCE(p); \
+	smp_read_barrier_depends(); /* Dependency order vs. p above. */ \
+	(_________p1); \
+})
+
+/**
  * rcu_assign_pointer() - assign to RCU-protected pointer
  * @p: pointer to assign to
  * @v: value to assign (publish)
diff -uNr a/include/linux/fs.h b/include/linux/fs.h
--- a/include/linux/fs.h	2015-06-29 19:18:02.680435246 +0300
+++ b/include/linux/fs.h	2015-06-29 19:16:57.689434547 +0300
@@ -2857,4 +2857,10 @@
 	return !IS_DEADDIR(inode);
 }
 
+/*
+ * Maximum number of layers of fs stack.  Needs to be limited to
+ * prevent kernel stack overflow
+ */
+#define FILESYSTEM_MAX_STACK_DEPTH 2
+
 #endif /* _LINUX_FS_H */
diff -uNr a/include/linux/fs.h b/include/linux/fs.h
--- a/include/linux/fs.h	2015-06-29 19:26:43.086440843 +0300
+++ b/include/linux/fs.h	2015-06-29 19:26:07.111440456 +0300
@@ -2863,4 +2863,11 @@
  */
 #define FILESYSTEM_MAX_STACK_DEPTH 2
 
+/*
+ * Whiteout is represented by a char device.  The following constants define the
+ * mode and device number to use.
+ */
+#define WHITEOUT_MODE 0
+#define WHITEOUT_DEV 0
+
 #endif /* _LINUX_FS_H */
 
diff -uNr a/fs/overlayfs/inode.c b/fs/overlayfs/inode.c
--- a/fs/overlayfs/inode.c	2015-06-30 11:44:27.964060252 +0300
+++ b/fs/overlayfs/inode.c	2015-06-30 11:44:12.969060091 +0300
@@ -140,12 +140,11 @@
 	void *cookie;
 };
 
-static const char *ovl_follow_link(struct dentry *dentry, void **cookie)
+static void *ovl_follow_link(struct dentry *dentry, struct nameidata *nd)
 {
+	void *ret;
 	struct dentry *realdentry;
 	struct inode *realinode;
-	struct ovl_link_data *data = NULL;
-	const char *ret;
 
 	realdentry = ovl_dentry_real(dentry);
 	realinode = realdentry->d_inode;
@@ -153,28 +152,28 @@
 	if (WARN_ON(!realinode->i_op->follow_link))
 		return ERR_PTR(-EPERM);
 
+	ret = realinode->i_op->follow_link(realdentry, nd);
+	if (IS_ERR(ret))
+		return ret;
+
 	if (realinode->i_op->put_link) {
+		struct ovl_link_data *data;
+
 		data = kmalloc(sizeof(struct ovl_link_data), GFP_KERNEL);
-		if (!data)
+		if (!data) {
+			realinode->i_op->put_link(realdentry, nd, ret);
 			return ERR_PTR(-ENOMEM);
+		}
 		data->realdentry = realdentry;
-	}
+		data->cookie = ret;
 
-	ret = realinode->i_op->follow_link(realdentry, cookie);
-	if (IS_ERR_OR_NULL(ret)) {
-		kfree(data);
-		return ret;
+		return data;
+	} else {
+		return NULL;
 	}
-
-	if (data)
-		data->cookie = *cookie;
-
-	*cookie = data;
-
-	return ret;
 }
 
-static void ovl_put_link(struct inode *unused, void *c)
+static void ovl_put_link(struct dentry *dentry, struct nameidata *nd, void *c)
 {
 	struct inode *realinode;
 	struct ovl_link_data *data = c;
@@ -183,7 +182,7 @@
 		return;
 
 	realinode = data->realdentry->d_inode;
-	realinode->i_op->put_link(realinode, data->cookie);
+	realinode->i_op->put_link(data->realdentry, nd, data->cookie);
 	kfree(data);
 }
 
 
diff -uNr a/fs/namei.c b/fs/namei.c
--- a/fs/namei.c	2015-06-30 12:14:01.990079333 +0300
+++ b/fs/namei.c	2015-06-30 12:12:14.190078174 +0300
@@ -2361,22 +2361,17 @@
 }
 EXPORT_SYMBOL(kern_path_mountpoint);
 
-/*
- * It's inline, so penalty for filesystems that don't use sticky bit is
- * minimal.
- */
-static inline int check_sticky(struct inode *dir, struct inode *inode)
+int __check_sticky(struct inode *dir, struct inode *inode)
 {
 	kuid_t fsuid = current_fsuid();
 
-	if (!(dir->i_mode & S_ISVTX))
-		return 0;
 	if (uid_eq(inode->i_uid, fsuid))
 		return 0;
 	if (uid_eq(dir->i_uid, fsuid))
 		return 0;
 	return !capable_wrt_inode_uidgid(inode, CAP_FOWNER);
 }
+EXPORT_SYMBOL(__check_sticky);
 
 /*
  *	Check whether we can remove a link victim from directory dir, check
diff -uNr a/include/linux/fs.h b/include/linux/fs.h
--- a/include/linux/fs.h	2015-06-30 12:14:03.616079351 +0300
+++ b/include/linux/fs.h	2015-06-30 12:13:14.321078820 +0300
@@ -2324,6 +2324,7 @@
 extern int inode_permission(struct inode *, int);
 extern int __inode_permission(struct inode *, int);
 extern int generic_permission(struct inode *, int);
+extern int __check_sticky(struct inode *dir, struct inode *inode);
 
 static inline bool execute_ok(struct inode *inode)
 {
@@ -2814,6 +2815,14 @@
 	return (mode & S_ISUID) || ((mode & S_ISGID) && (mode & S_IXGRP));
 }
 
+static inline int check_sticky(struct inode *dir, struct inode *inode)
+{
+	if (!(dir->i_mode & S_ISVTX))
+		return 0;
+
+	return __check_sticky(dir, inode);
+}
+
 static inline void inode_has_no_xattr(struct inode *inode)
 {
 	if (!is_sxid(inode->i_mode) && (inode->i_sb->s_flags & MS_NOSEC))
diff -uNr a/include/uapi/linux/fs.h b/include/uapi/linux/fs.h
--- a/include/uapi/linux/fs.h	2014-03-31 07:40:15.000000000 +0400
+++ b/include/uapi/linux/fs.h	2015-06-30 12:06:07.128074226 +0300
@@ -201,4 +201,8 @@
 #define SYNC_FILE_RANGE_WRITE		2
 #define SYNC_FILE_RANGE_WAIT_AFTER	4
 
+#define RENAME_NOREPLACE	(1 << 0)	/* Don't overwrite target */
+#define RENAME_EXCHANGE		(1 << 1)	/* Exchange source and dest */
+#define RENAME_WHITEOUT		(1 << 2)	/* Whiteout source */
+
 #endif /* _UAPI_LINUX_FS_H */

diff -uNr a/fs/overlayfs/dir.c b/fs/overlayfs/dir.c
--- a/fs/overlayfs/dir.c	2015-06-30 12:34:04.449092266 +0300
+++ b/fs/overlayfs/dir.c	2015-06-30 12:31:51.785090840 +0300
@@ -676,11 +676,11 @@
 	return ovl_do_remove(dentry, true);
 }
 
-static int ovl_rename2(struct inode *olddir, struct dentry *old,
-		       struct inode *newdir, struct dentry *new,
-		       unsigned int flags)
+static int ovl_rename(struct inode *olddir, struct dentry *old,
+		       struct inode *newdir, struct dentry *new)
 {
 	int err;
+	unsigned int flags;
 	enum ovl_path_type old_type;
 	enum ovl_path_type new_type;
 	struct dentry *old_upperdir;
@@ -914,7 +914,7 @@
 	.symlink	= ovl_symlink,
 	.unlink		= ovl_unlink,
 	.rmdir		= ovl_rmdir,
-	.rename2	= ovl_rename2,
+	.rename		= ovl_rename,
 	.link		= ovl_link,
 	.setattr	= ovl_setattr,
 	.create		= ovl_create,

diff -uNr a/fs/namei.c b/fs/namei.c
--- a/fs/namei.c	2015-06-30 13:29:10.473127825 +0300
+++ b/fs/namei.c	2015-06-30 13:28:46.550127568 +0300
@@ -4281,6 +4281,20 @@
 	return sys_renameat(AT_FDCWD, oldname, AT_FDCWD, newname);
 }
 
+int vfs_whiteout(struct inode *dir, struct dentry *dentry)
+{
+	int error = may_create(dir, dentry);
+	if (error)
+		return error;
+
+	if (!dir->i_op->mknod)
+		return -EPERM;
+
+	return dir->i_op->mknod(dir, dentry,
+				S_IFCHR | WHITEOUT_MODE, WHITEOUT_DEV);
+}
+EXPORT_SYMBOL(vfs_whiteout);
+
 int vfs_readlink(struct dentry *dentry, char __user *buffer, int buflen, const char *link)
 {
 	int len;

diff -uNr a/fs/overlayfs/copy_up.c b/fs/overlayfs/copy_up.c
--- a/fs/overlayfs/copy_up.c	2015-07-02 20:43:58.660311125 +0300
+++ b/fs/overlayfs/copy_up.c	2015-07-02 20:22:18.318297139 +0300
@@ -58,14 +58,6 @@
 			error = size;
 			goto out_free_value;
 		}
-		error = security_inode_copy_up_xattr(old, new,
-						     name, value, &size);
-		if (error < 0)
-			goto out_free_value;
-		if (error == 1) {
-			error = 0;
-			continue; /* Discard */
-		}
 		error = vfs_setxattr(new, name, value, size, 0);
 		if (error)
 			goto out_free_value;
@@ -231,10 +223,6 @@
 	if (err)
 		goto out2;
 
-	err = security_inode_copy_up(lowerpath->dentry, newdentry);
-	if (err < 0)
-		goto out_cleanup;
-
 	if (S_ISREG(stat->mode)) {
 		struct path upperpath;
 		ovl_path_upper(dentry, &upperpath);
diff -uNr a/fs/overlayfs/inode.c b/fs/overlayfs/inode.c
--- a/fs/overlayfs/inode.c	2015-07-02 20:43:58.666311125 +0300
+++ b/fs/overlayfs/inode.c	2015-07-02 20:43:17.814310685 +0300
@@ -336,30 +336,38 @@
 	return true;
 }
 
-struct inode *ovl_d_select_inode(struct dentry *dentry, unsigned file_flags)
+ 
+static int ovl_dentry_open(struct dentry *dentry, struct file *file,
+		    const struct cred *cred)
 {
 	int err;
 	struct path realpath;
 	enum ovl_path_type type;
+	bool want_write = false;
 
 	type = ovl_path_real(dentry, &realpath);
-	if (ovl_open_need_copy_up(file_flags, type, realpath.dentry)) {
+	if (ovl_open_need_copy_up(file->f_flags, type, realpath.dentry)) {
+		want_write = true;
 		err = ovl_want_write(dentry);
 		if (err)
-			return ERR_PTR(err);
+			goto out;
 
-		if (file_flags & O_TRUNC)
+		if (file->f_flags & O_TRUNC)
 			err = ovl_copy_up_last(dentry, NULL, true);
 		else
 			err = ovl_copy_up(dentry);
-		ovl_drop_write(dentry);
 		if (err)
-			return ERR_PTR(err);
+			goto out_drop_write;
 
 		ovl_path_upper(dentry, &realpath);
 	}
 
-	return d_backing_inode(realpath.dentry);
+	err = vfs_open(&realpath, file, cred);
+out_drop_write:
+	if (want_write)
+		ovl_drop_write(dentry);
+out:
+	return err;
 }
 
 static const struct inode_operations ovl_file_inode_operations = {
@@ -370,6 +378,7 @@
 	.getxattr	= ovl_getxattr,
 	.listxattr	= ovl_listxattr,
 	.removexattr	= ovl_removexattr,
+	.dentry_open	= ovl_dentry_open,
 };
 
 static const struct inode_operations ovl_symlink_inode_operations = {
diff -uNr a/fs/overlayfs/overlayfs.h b/fs/overlayfs/overlayfs.h
--- a/fs/overlayfs/overlayfs.h	2015-07-02 20:43:58.661311125 +0300
+++ b/fs/overlayfs/overlayfs.h	2015-07-02 20:20:40.405296086 +0300
@@ -173,7 +173,6 @@
 		     void *value, size_t size);
 ssize_t ovl_listxattr(struct dentry *dentry, char *list, size_t size);
 int ovl_removexattr(struct dentry *dentry, const char *name);
-struct inode *ovl_d_select_inode(struct dentry *dentry, unsigned file_flags);
 
 struct inode *ovl_new_inode(struct super_block *sb, umode_t mode,
 			    struct ovl_entry *oe);
diff -uNr a/fs/overlayfs/super.c b/fs/overlayfs/super.c
--- a/fs/overlayfs/super.c	2015-07-02 20:43:58.662311125 +0300
+++ b/fs/overlayfs/super.c	2015-07-02 20:21:04.044296340 +0300
@@ -275,7 +275,6 @@
 
 static const struct dentry_operations ovl_dentry_operations = {
 	.d_release = ovl_dentry_release,
-	.d_select_inode = ovl_d_select_inode,
 };
 
 static struct ovl_entry *ovl_alloc_entry(unsigned int numlower)

